//Destructuracion de objetos y arreglos

let avenger ={

    nombre: "Steve",
    clave: "Capitan America",
    poder: "Droga"
}


let {nombre, clave, poder} = avenger;
//let nombre= avenger.nombre;
//let clave = avenger.poder;
//let poder = avenger.poder;



console.log(nombre, clave, poder);



let avengers:string[] = ["Thor", "Steve","Tony"];

let [thor , capi, ironman] = avengers;

console.log(thor, capi, ironman);