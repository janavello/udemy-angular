var prom1 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        console.log("Promesa terminada");
        //termina bien
        resolve();
        //termina mal
        //reject();
    }, 1500);
});
prom1.then(function () {
    console.log("Ejecutarme cuando se termine bien!");
}, function () {
    console.error("ejecutar si todo sale mal");
});
