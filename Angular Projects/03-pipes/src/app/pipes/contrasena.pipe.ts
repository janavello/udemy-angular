import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'contrasena' })
export class ContrasenaPipe implements PipeTransform {
  transform(value: string, activar: boolean): string {
    if (activar == true) {
      let largo: number = value.length;
      let clave: string = '';

      for (let index = 0; index < largo; index++) {
        clave = clave + '*';
      }

      value = value.replace(value, clave);
    }

    return value;
  }
}
