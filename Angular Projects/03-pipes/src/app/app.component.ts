import { Component } from '@angular/core';
import { resolve, reject } from 'q';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  nombre = 'Jancarlos';

  activar: boolean = true;

  nombre2 = 'jancARlos jAVier aveLLo sOtO';

  arreglo = [1, 2, 3, 4, 5, 6, 7, 8 , 9, 10];

  PI = Math.PI;

  a = 0.234;

  salario = 1234.5;

  video = 'Aa_AFP_86y4';

  heroe = {
    nombre: 'Logan',
    clave: 'Wolverine',
    edad: 500,
    direccion: {
      calle: 'primera',
      casa: 123
    }


  }

   valorDePromesa = new Promise( ( resolve, reject) =>{

    setTimeout( ()=> resolve('Llego la data'), 3500 );

   }) ;



fecha = new Date();

}
